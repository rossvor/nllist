#!/bin/bash

# setup keychain
eval $(keychain --eval id_rsa)

# bail if anything fails
set -e

nllist="$( dirname "$0" )/.." 
cd "$nllist"
datapath="$nllist/data"

function appendOnlyPatch() {
    local fpath="$datapath/$1.json"
    local tmpfile=$2
    # get diff and transform it to make it append-only
    patch $fpath \
        <(diff $fpath $tmpfile \
        | sed -z "s|[0-9a-f]\{1,10\},\{0,1\}[0-9a-f]\{0,10\}\n<|\n<|g" \
        | grep -v "^<" | grep -v "^$")
}

printf "Fetching geoguessr season 3...\n"
tmp=$(mktemp)
"$HOME/.local/bin/plz" --json \
    --first-regex="with Sinvicta - Episode ([0-9]{1,2})" \
    --second-regex="#([0-9]{1,2}) \[Season 3\]" \
    "PL1bauNEiHIgyztJ6fuEdXyJDpHO2X-tmv" "PLb3dMxJh3FDHW053OUJP9n1IDbv-VPpVN" \
    | jq > $tmp
appendOnlyPatch "geoguessr3" "$tmp"

git add -u .
curdate=$( date -u +"%Y-%m-%dT%H:%M:%SZ" )
msg="Update episode lists on $curdate"
git commit -m "$msg"
git push origin master


