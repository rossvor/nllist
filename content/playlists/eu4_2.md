---
title: "Europa Universalis IV - part 2"
short_title: "EU IV - part 2"
draft: false
sources: 
    1:
        title: "Wealth of Nations"
        source: eu4_wealth_of_nations
    2:
        title: "Eastern Promises"
        source: eu4_eastern_promises
---
☙ _Various combinations of NL, Arumba, Quill and Mathas playing EU4._ ❧
