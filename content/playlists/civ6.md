---
title: "Civilization VI"
short_title: "Civilization VI"
draft: false
sources: 
    1:
        title: "Season 1"
        source: civvi_2
    2:
        title: "Season 2"
        source: civvi_2
---
☙ _Featuring NL, Baer, Austin, DG and Sinvicta playing Civ 6._ ❧
