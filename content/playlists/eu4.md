---
title: "Europa Universalis IV - part 1"
short_title: "EU IV - part 1"
draft: false
sources: 
    1:
        title: "El Dorado"
        source: eu4_el_dorado
    2:
        title: "Common Sense"
        source: eu4_common_sense
    3:
        title: "Art of War"
        source: eu4_art_of_war
---
☙ _Various combinations of NL, Arumba, Quill and Mathas playing EU4._ ❧
